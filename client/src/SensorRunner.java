import java.io.*;
import java.net.*;
import java.util.Random;
import java.util.Scanner;

public class SensorRunner {
    long startTime;
    int neighbourPort = 0;
    String neighbourIP = "";
    int temperature = -1;
    int pressure = -1;
    int humidity = -1;
    int CO = -1;
    int NO2 = -1;
    int SO2 = -1;
    Sensor sensor;
    Measurement measurement = null;

    private ServerSocket serverSocket;
    private Socket socket;
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;

    public SensorRunner() {
        startTime = System.nanoTime();
        String name = "Sensor " + new Random().nextInt()%30;
        System.out.println(name);
        String IPAddress = "127.0.0.1";
        int port = Math.abs(new Random().nextInt()%9999);
        double latitude = 45.75 + (45.85 - 45.75) * new Random().nextDouble();
        double longitude = 15.87 + (16 - 15.87) * new Random().nextDouble();

        sensor = new Sensor(name, latitude, longitude, IPAddress, port);
        try {
            registerSensor(sensor);
        } catch (IOException e) {
            e.printStackTrace();
        }
        new Thread(() -> {
            try {
                runClient();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(() -> {
            try {
                runServer();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void runServer() throws IOException {
        serverSocket = new ServerSocket(sensor.getPort(), 15);
        waitForConnection();
        setupStreams();
    }

    private void setupStreams() throws IOException {
        outputStream = new ObjectOutputStream(socket.getOutputStream());
        outputStream.flush();
        outputStream.writeObject(measurement);
        outputStream.flush();
        outputStream.close();
        serverSocket.close();
        runServer();
    }

    private void waitForConnection() throws IOException {
        socket = serverSocket.accept();
        System.out.println("Now connected to " + socket.getInetAddress().getHostName());
    }

    private void runClient() throws IOException, ClassNotFoundException {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()){
            String line = scanner.nextLine();
            if(line.trim().toLowerCase().equals("stop"))
                return;
            if(line.trim().toLowerCase().equals("start"))
                handleMeasurement();
        }
    }

    private void handleMeasurement() throws IOException, ClassNotFoundException {
        long endTime = System.nanoTime();
        double difference = ((endTime - startTime)/(Math.pow(10, 10)));
        difference = Math.round(difference);
        System.out.println("here");
        System.out.println("Active seconds " + difference);
        int lineToRead = (int) difference%100 + 2;
        System.out.println("Preparing to read line number " + lineToRead);
        String measurementLine = "";
        try {
            Scanner scanner = new Scanner(new FileInputStream("C:\\Users\\lukak\\Documents\\rassus-homework-1-client\\client\\src\\mjerenja.csv"));
            int i = 1;
            while(scanner.hasNextLine()){
                if(i == lineToRead){
                    measurementLine = scanner.nextLine();
                    break;
                }
                ++i;
                scanner.nextLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Line: " + measurementLine);
        String[] lineArray = measurementLine.split(",");
        temperature = Integer.parseInt(lineArray[0]);
        pressure = Integer.parseInt(lineArray[1]);
        humidity = Integer.parseInt(lineArray[2]);
        if(!lineArray[3].isEmpty())
            CO = Integer.parseInt(lineArray[3]);
        if(!lineArray[4].isEmpty())
            NO2 = Integer.parseInt(lineArray[4]);
        if(!(lineArray.length < 6))
            NO2 = Integer.parseInt(lineArray[5]);
        System.out.println("CO: " + CO);
        System.out.println("NO2: " + NO2);
        System.out.println("SO2: " + SO2);
        fetchNeighbour();
        measurement = new Measurement(temperature,pressure,humidity, CO, NO2, SO2);
        Measurement neighbourMeasurement;

        Socket socket = new Socket(InetAddress.getByName(neighbourIP), neighbourPort);
        ObjectInputStream objectInputStreamSocket = new ObjectInputStream(socket.getInputStream());
        neighbourMeasurement = (Measurement) objectInputStreamSocket.readObject();
        System.out.println("Fetched: " + neighbourMeasurement);
        objectInputStreamSocket.close();

        String url = "http://localhost:8090/storeMeasurements";
        java.net.URL obj = new URL(url);
        HttpURLConnection httpURLConnection = (HttpURLConnection) obj.openConnection();
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Content-Type", "application/json");
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);

        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
        int temperature = measurement.getTemperature();
        int pressure = measurement.getPressure();
        int humidity = measurement.getHumidity();
        int CO = measurement.getCO();
        int NO2 = measurement.getNO2();
        int SO2 = measurement.getSO2();
        if(neighbourMeasurement != null) {
            temperature = (measurement.getTemperature() + neighbourMeasurement.getTemperature()) / 2;
            pressure = (measurement.getPressure() + neighbourMeasurement.getPressure()) / 2;
            humidity = (measurement.getHumidity() + neighbourMeasurement.getHumidity()) / 2;
            if(measurement.getCO() == -1 || measurement.getCO() == 0)
                CO = neighbourMeasurement.getCO();
            else if(!(neighbourMeasurement.getCO() == -1 || neighbourMeasurement.getCO() == 0))
                CO = (measurement.getCO() + neighbourMeasurement.getCO()) / 2;
            if(measurement.getNO2() == -1 || measurement.getNO2() == 0)
                NO2 = neighbourMeasurement.getNO2();
            else if(!(neighbourMeasurement.getNO2() == -1 || neighbourMeasurement.getNO2() == 0))
                NO2 = (measurement.getNO2() + neighbourMeasurement.getNO2()) / 2;
            if(measurement.getSO2() == -1 || measurement.getSO2() == 0)
                SO2 = neighbourMeasurement.getSO2();
            else if(!(neighbourMeasurement.getSO2() == -1 || neighbourMeasurement.getSO2() == 0))
                SO2 = (measurement.getSO2() + neighbourMeasurement.getSO2()) / 2;
        }
        outputStreamWriter.write("{\n" +
                "\t\"username\":\"" + sensor.getUsername() + "\",\n" +
                "\t\"parameter\":\"Temeperature\",\n" +
                "\t\"value\":" + temperature + "\n" +
                "}");
        outputStreamWriter.flush();
        outputStreamWriter.close();
        System.out.println(httpURLConnection.getResponseCode());

        httpURLConnection = (HttpURLConnection) obj.openConnection();
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Content-Type", "application/json");
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);

        outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
        outputStreamWriter.write("{\n" +
                "\t\"username\":\"" + sensor.getUsername() + "\",\n" +
                "\t\"parameter\":\"Pressure\",\n" +
                "\t\"value\":" + pressure + "\n" +
                "}");
        outputStreamWriter.flush();
        outputStreamWriter.close();
        System.out.println(httpURLConnection.getResponseCode());

        httpURLConnection = (HttpURLConnection) obj.openConnection();
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Content-Type", "application/json");
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);

        outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
        outputStreamWriter.write("{\n" +
                "\t\"username\":\"" + sensor.getUsername() + "\",\n" +
                "\t\"parameter\":\"Humidity\",\n" +
                "\t\"value\":" + humidity + "\n" +
                "}");
        outputStreamWriter.flush();
        outputStreamWriter.close();
        System.out.println(httpURLConnection.getResponseCode());

        httpURLConnection = (HttpURLConnection) obj.openConnection();
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Content-Type", "application/json");
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);

        outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
        outputStreamWriter.write("{\n" +
                "\t\"username\":\"" + sensor.getUsername() + "\",\n" +
                "\t\"parameter\":\"CO\",\n" +
                "\t\"value\":" + CO + "\n" +
                "}");
        outputStreamWriter.flush();
        outputStreamWriter.close();
        System.out.println(httpURLConnection.getResponseCode());

        httpURLConnection = (HttpURLConnection) obj.openConnection();
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Content-Type", "application/json");
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);

        outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
        outputStreamWriter.write("{\n" +
                "\t\"username\":\"" + sensor.getUsername() + "\",\n" +
                "\t\"parameter\":\"NO2\",\n" +
                "\t\"value\":" + NO2 + "\n" +
                "}");
        outputStreamWriter.flush();
        outputStreamWriter.close();
        System.out.println(httpURLConnection.getResponseCode());

        httpURLConnection = (HttpURLConnection) obj.openConnection();
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Content-Type", "application/json");
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);

        outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
        outputStreamWriter.write("{\n" +
                "\t\"username\":\"" + sensor.getUsername() + "\",\n" +
                "\t\"parameter\":\"SO2\",\n" +
                "\t\"value\":" + SO2 + "\n" +
                "}");
        outputStreamWriter.flush();
        outputStreamWriter.close();
        System.out.println(httpURLConnection.getResponseCode());
    }

    private void registerSensor(Sensor sensor) throws IOException {

        String url = "http://localhost:8090/register";
        java.net.URL obj = new URL(url);
        HttpURLConnection httpURLConnection = (HttpURLConnection) obj.openConnection();
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Content-Type", "application/json");
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);

        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
        outputStreamWriter.write("{\n" +
                "\t\"username\":\"" + sensor.getUsername() + "\",\n" +
                "\t\"latitude\":" + sensor.getLatitude() + ",\n" +
                "\t\"longitude\":" + sensor.getLongitude() + ",\n" +
                "\t\"ipaddress\":\"" + sensor.getIpadress() + "\",\n" +
                "\t\"port\":" + sensor.getPort() + "\n" +
                "}");
        outputStreamWriter.flush();
        outputStreamWriter.close();
        System.out.println(httpURLConnection.getResponseCode());

        fetchNeighbour();
    }

    private void fetchNeighbour() throws IOException {
        String url = "http://localhost:8090/findNeighbour";
        java.net.URL obj = new URL(url);
        HttpURLConnection httpURLConnection = (HttpURLConnection) obj.openConnection();
        httpURLConnection.setRequestMethod("GET");
        httpURLConnection.setRequestProperty("Content-Type", "application/json");
        httpURLConnection.setRequestProperty("username", sensor.getUsername());
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);

        InputStream inputStream = httpURLConnection.getInputStream();
        Scanner scanner = new Scanner(inputStream);
        while(scanner.hasNextLine()){
            String line = scanner.nextLine();
            line = line.replace(":", "").replace(",", "\n");
            String[] lines = line.split("\n");
            neighbourPort = Integer.parseInt(lines[0].split("\"")[2]);
            neighbourIP = lines[1].split("\"")[3];
        }
        System.out.println(neighbourIP);
        System.out.println(neighbourPort);
    }
}
