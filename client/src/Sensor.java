public class Sensor {
    String username;
    double latitude;
    double longitude;
    String ipadress;
    int port;

    public Sensor() {
    }

    public Sensor(String username, double latitude, double longitude, String ipadress, int port) {
        this.username = username;
        this.latitude = latitude;
        this.longitude = longitude;
        this.ipadress = ipadress;
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getIpadress() {
        return ipadress;
    }

    public void setIpadress(String ipadress) {
        this.ipadress = ipadress;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "Sensor{" +
                "username='" + username + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", ipadress='" + ipadress + '\'' +
                ", port=" + port +
                '}';
    }
}
