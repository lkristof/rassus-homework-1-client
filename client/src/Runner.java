import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Runner {

    public static void main (String[] args) throws InterruptedException {
        List<SensorRunner> listOfRunners = new ArrayList<>();
        int i = 1;
        while(i < 2){
            new Thread(() -> {
                listOfRunners.add(new SensorRunner());
            }).start();
            ++i;
            Thread.sleep(5000);
        }

    }

}
